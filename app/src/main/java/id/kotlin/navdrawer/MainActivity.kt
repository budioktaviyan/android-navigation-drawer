package id.kotlin.navdrawer

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnNavigationItemSelectedListener {

  private lateinit var navigation: NavController

  private var currenMenuItem: Int = -1
  private var currentNavigation: Int = -1

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setSupportActionBar(toolbar_main)

    navigation = Navigation.findNavController(this, R.id.nav_main)
    NavigationUI.setupActionBarWithNavController(this, navigation, dl_main)
    NavigationUI.setupWithNavController(nv_main, navigation)

    nv_main.setNavigationItemSelectedListener(this)
  }

  override fun onBackPressed() {
    when {
      dl_main.isDrawerOpen(GravityCompat.START) -> dl_main.closeDrawers()
      else -> {
        if (currentNavigation == R.id.fragment_home) {
          supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
          super.onBackPressed()
        } else {
          supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
          navigation.navigate(R.id.fragment_home)
        }

        super.onBackPressed()
      }
    }
  }

  override fun onSupportNavigateUp(): Boolean = NavigationUI.navigateUp(navigation, dl_main)

  override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
    menuItem.isChecked = false
    dl_main.closeDrawers()

    val id = menuItem.itemId
    if (id == currenMenuItem) return false

    return when (id) {
      R.id.navigation_home -> {
        navigation.navigate(R.id.fragment_home)
        currenMenuItem = id
        currentNavigation = R.id.fragment_home
        true
      }
      R.id.navigation_favorite -> {
        navigation.navigate(R.id.fragment_favorite)
        currenMenuItem = id
        currentNavigation = R.id.fragment_favorite
        true
      }
      R.id.navigation_settings -> {
        navigation.navigate(R.id.fragment_settings)
        currenMenuItem = id
        currentNavigation = R.id.fragment_settings
        true
      }
      else -> {
        currenMenuItem = id
        currentNavigation = navigation.currentDestination?.id ?: -1
        false
      }
    }
  }
}